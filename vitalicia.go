package main

import (
	"database/sql"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"maunium.net/go/mautrix"
	mevent "maunium.net/go/mautrix/event"
	"maunium.net/go/mautrix/id"
)

var whatsAppBotRoomId string = ""
var whatsappBotMutex sync.Mutex = sync.Mutex{}

var whatsAppMatrixUserRegexp regexp.Regexp = *regexp.MustCompile(`^(@)[a-zA-Z-_]+[a-zA-Z]{1}(_)\d+(:)[\.\d\w-_]+\w$`)
var whatsAppMatrixPhoneNumberRegexp regexp.Regexp = *regexp.MustCompile(`(_)\d*(:)`)
var phoneNumberRegexp regexp.Regexp = *regexp.MustCompile(`([0-9])+`)

type Vitalicia struct {
}

func NewVitalicia() *Vitalicia {
	return &Vitalicia{}
}

type StateContent struct {
	Membership string `json:"membership"`
	BridgeBot  string `json:"bridgebot"`
}

type State struct {
	Content StateContent `json:"content"`
	UserId  string       `json:"user_id"`
	Type    string       `json:"type"`
}

type Room struct {
	RoomId     string  `json:"room_id"`
	Membership string  `json:"membership"`
	Visibility string  `json:"visibility"`
	Inviter    string  `json:"inviter"`
	States     []State `json:"state"`
}

type InitialSync struct {
	Rooms []Room `json:"rooms"`
}

type syncParamsType struct {
	accessToken  string
	matrixDomain string
}

type MatrixLoginResponse struct {
	user_id      string
	access_token string
	home_server  string
	device_id    string
}

func (vitalicia *Vitalicia) HealthCheck(w http.ResponseWriter, r *http.Request) {
	log.Info("Health check")
	w.Write([]byte("Matrix Chatwoot Bridge health check"))
	return
}

func extractBody(content io.ReadCloser) ([]byte, error) {
	var body []byte
	var err error
	if body, err = ioutil.ReadAll(content); err != nil {
		return nil, err
	}
	return body, nil
}

func setAccessToken(accessToken string, httpRequest http.Request) {
	httpRequest.Header.Add("Authorization", "Bearer "+accessToken)
}

func unmarshal(content io.ReadCloser, v interface{}) error {
	body, err := extractBody(content)
	if err != nil {
		return err
	}
	return json.Unmarshal(body, v)
}

func logout(accessToken string, matrixDomain string) {
	req, err := http.NewRequest("POST", "https://"+matrixDomain+"/_matrix/client/r0/logout", strings.NewReader("{}"))
	if err != nil {
		log.Fatal(err)
	}
	setAccessToken(accessToken, *req)

	// req.Header.Add("Authorization", "Bearer "+accessToken)
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Fatal(err)
	} else if resp.StatusCode == 200 {
		log.Println("Log out done.")
	} else {
		log.Println("Error: did not log out - status code:", resp.StatusCode)
	}

}

type StartChatWithPhoneNumberType struct {
	MatrixUser            string
	MatrixAccessToken     string
	MatrixUrl             string
	WhatsappBotMatrixUser string
	ChatwootBotMatrixUser string
}

type MatrixMessage struct {
	Body    string `json:"body"`
	MsgType string `json:"msgtype"`
}

func SendMessageToRoom(matrixAccessToken string, matrixUrl string, matrixRoomId string, messageContent string) error {
	message := MatrixMessage{
		Body:    messageContent,
		MsgType: "m.text",
	}

	var err error

	log.Println("Sending message to room", matrixRoomId)
	var jsonByteArray []byte
	if jsonByteArray, err = json.Marshal(message); err != nil {
		log.Fatalln(err)
	}
	jsonStr := string(jsonByteArray)

	var urlInstance *url.URL
	if urlInstance, err = url.Parse(matrixUrl + "/_matrix/client/r0/rooms/" + matrixRoomId + "/send/m.room.message?access_token=" + matrixAccessToken); err != nil {
		return err
	}
	urlStr := urlInstance.String()

	// POST request
	var req *http.Request
	if req, err = http.NewRequest("POST", urlStr, strings.NewReader(jsonStr)); err != nil {
		return err
	}

	var resp *http.Response
	if resp, err = http.DefaultClient.Do(req); err != nil {
		return err
	}

	if resp.StatusCode != 200 {
		if bodyBytes, err := ioutil.ReadAll(resp.Body); err == nil {
			log.Fatal(string(bodyBytes))
		} else {
			log.Fatal(err)
		}
	}

	return nil
}

func syncMatrix() (*mautrix.RespSync, error) {
	var respSync *mautrix.RespSync
	var err error
	if respSync, err = client.SyncRequest(1, "", "", true, mevent.PresenceOffline, nil); err != nil {
		return nil, err
	}
	
	return respSync, nil
}

func vitaliciaStartChatWithCustomer(params StartChatWithPhoneNumberType) (string, error) {

	var err error

	if err = setWhatsAppBotRoomId(); err != nil {
		return "", err
	}

	whatsappbotRoomID := id.RoomID(whatsAppBotRoomId)
	if _, found := roomSendlocks[whatsappbotRoomID]; !found {
		roomSendlocks[whatsappbotRoomID] = &sync.Mutex{}
	}
	roomSendlocks[whatsappbotRoomID].Lock()
	defer roomSendlocks[whatsappbotRoomID].Unlock()

	log.Printf("Creating portal in Matrix: pm %s", params.MatrixUser)
	if _, err = client.SendText(whatsappbotRoomID, "pm "+params.MatrixUser); err != nil {
		log.Println("Error:", err)
	}
	log.Println("Give Matrix some time to process the pm command as the response is asynchronous.")
	time.Sleep(time.Second * 1)

	if !whatsAppMatrixUserRegexp.MatchString(params.MatrixUser) {
		return "", errors.New("MIXD is not a WhatsApp Bridge user")
	}

	customerPhoneNumber := whatsAppMatrixPhoneNumberRegexp.FindString(params.MatrixUser)

	return qwer(whatsappbotRoomID, customerPhoneNumber[1:len(customerPhoneNumber)-1])
}

func qwer(whatsappbotRoomID id.RoomID, customerPhoneNumber string) (string, error) {
	retry := 0
	foundPmCommand := false
	foundNotice := false
	lastStatusMsg := ""
	roomId := ""
	var err error
	// Regexp to extract the room ID from the message body.
	roomIdRegexp := regexp.MustCompile(`(!)[A-Za-z]*(:).*(vitalicia.io)`)
	for {
		foundPmCommand = false
		foundNotice = false
		lastStatusMsg = ""
		roomId = ""

		var resp *mautrix.RespSync
		// TODO use method syncMatrix()
		resp, err = client.SyncRequest(1, "", "", false, mevent.PresenceOffline, nil)
		if err != nil {
			return "", err
		}

		var room mautrix.SyncJoinedRoom
		for key, r := range resp.Rooms.Join {
			if key == whatsappbotRoomID {
				room = r
				break
			}
		}

		for i := len(room.Timeline.Events) - 1; i >= 0; i-- {
			mEvent := room.Timeline.Events[i]
			if lastStatusMsg == "" {
				lastStatusMsg = mEvent.Content.Raw["body"].(string)
			}
			switch mEvent.Content.Raw["msgtype"] {
			case "m.text":
				if strings.Index(mEvent.Content.Raw["body"].(string), "pm +"+customerPhoneNumber) >= 0 {
					foundPmCommand = true
				}
				break
			case "m.notice":
				if !foundPmCommand {
					if strings.Index(mEvent.Content.Raw["body"].(string), "You already have a private chat portal with +"+customerPhoneNumber) >= 0 {
						foundNotice = true
					}
				}
				break
			}

			if foundNotice {
				roomId = roomIdRegexp.FindString(mEvent.Content.Raw["body"].(string))

				// There may be a pending invite to the room; let's accept it.
				if _, err = client.JoinRoom(roomId, "", nil); err != nil {
					log.Println(err)
				}
				break
			} else if foundPmCommand {
				break
			}
		}

		if !foundPmCommand && !foundNotice {
			return "", errors.New("Last status message: " + lastStatusMsg)
		} else if foundNotice {
			return roomId, nil
		} else {
			retry++
			if retry > 5 {
				log.Println("Error: No reply from WhatsApp Bot. Aborted.")
				return "", errors.New("No reply from WhatsApp Bot. Aborted.")
			}
			log.Printf("No reply from WhatsApp Bot so far. Retry #%d", retry)
			time.Sleep(time.Second * 1)
		}
	}
}

func findWhatsappBotRoomId(whatsappBotUser string, chatwootBotUser string) string {
	var err error
	_whatsappBotRoomId := ""

	var respSync *mautrix.RespSync
	log.Println("Matrix sync request")
	if respSync, err = syncMatrix(); err != nil {
		return ""
	}

	for _, value := range respSync.AccountData.Events {
		if(value.Type.Type == "m.direct"){
			directChat := value.Content.Raw[whatsappBotUser].([]interface{})
			_whatsappBotRoomId = directChat[0].(string)
			break
		}	
	}

	return _whatsappBotRoomId
}

type FindUserRoomIdParams struct {
	chatwootBotUser   string
	whatsappBotUser   string
	user              string
	initialSync       InitialSync
	matrixDomain      string
	matrixAccessToken string
}

func acceptInvite(accessToken string, matrixDomain string, roomId string) {
	var err error
	var urlInstance *url.URL
	if urlInstance, err = url.Parse(matrixDomain + "/_matrix/client/r0/rooms/" + roomId + "/join"); err != nil {
		log.Fatal(err)
	}

	urlStr := urlInstance.String()
	log.Println("Request URL:", urlStr)
	var req *http.Request
	if req, err = http.NewRequest("POST", urlStr, strings.NewReader("{}")); err != nil {
		log.Fatal(err)
	}
	setAccessToken(accessToken, *req)

	if _, err := http.DefaultClient.Do(req); err != nil {
		log.Fatal(err)
	}
}

func findUserRoomId(params FindUserRoomIdParams) string {
	roomId := ""
	for _, room := range params.initialSync.Rooms {
		roomMemberCount := 0
		foundUser := false
		foundChatwootBot := false
		foundBridgeBot := false
		if room.Membership == "invite" && room.Inviter == params.user {
			log.Println("Accepting room invite automatically for room", room.RoomId)
			acceptInvite(params.matrixAccessToken, params.matrixDomain, room.RoomId)
			return room.RoomId
		} else if room.Membership == "join" && room.Visibility == "private" {
			for _, state := range room.States {
				if state.Type == "m.room.member" {
					roomMemberCount++
					if roomMemberCount > 2 {
						break
					}

					switch state.UserId {
					case params.chatwootBotUser:
						foundChatwootBot = true
						break
					case params.user:
						foundUser = true
						break
					default:
						break
					}
				} else if state.Type == "m.bridge" && state.Content.BridgeBot == params.whatsappBotUser {
					foundBridgeBot = true
				}
			}
		}

		if roomMemberCount == 2 && foundChatwootBot && foundUser && foundBridgeBot {
			roomId = room.RoomId
			break
		}
	}

	return roomId
}

func connectToChatwootDB() (*sql.DB, error) {
	var err error
	if chatwootDB != nil {
		if err = chatwootDB.Ping(); err != nil {
			return nil, err
		}
		return chatwootDB, nil
	}

	if chatwootDB, err = sql.Open("pgx", configuration.ChatwootDBConnectionString); err != nil {
		return nil, err
	}
	chatwootDB.SetMaxIdleConns(1)
	chatwootDB.SetMaxOpenConns(1)

	if err = chatwootDB.Ping(); err != nil {
		return nil, err
	}

	return chatwootDB, nil
}

func GetMatrixProfile(mxid string, client *mautrix.Client) (string, error) {
	var err error
	var displayName *mautrix.RespUserDisplayName

	if displayName, err = client.GetDisplayName(id.UserID(mxid)); err != nil {
		return "", err
	} else {
		return displayName.DisplayName, nil
	}
}

func setWhatsAppBotRoomId() error {

	whatsappBotMutex.Lock()
	defer whatsappBotMutex.Unlock()
	var err error

	if whatsAppBotRoomId == "" {
		
		whatsAppBotRoomId = findWhatsappBotRoomId(configuration.WhatsAppBotUser, configuration.Username)
		if whatsAppBotRoomId == "" {
			log.Fatal("Failed to find WhatsApp bot room ID. Was it created in Matrix?")
			return err
		}
	}

	return nil
}
