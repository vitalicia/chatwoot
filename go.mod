module gitlab.com/beeper/chatwoot

go 1.16

require (
	github.com/aws/aws-xray-sdk-go v1.7.0 // indirect
	github.com/go-playground/validator/v10 v10.11.0 // indirect
	github.com/jackc/pgx/v4 v4.13.0
	github.com/pkg/errors v0.9.1 // indirect
	github.com/sethvargo/go-retry v0.1.0
	github.com/sirupsen/logrus v1.8.1
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	maunium.net/go/mautrix v0.9.24
)
