package main

type Configuration struct {
	// Authentication settings
	Homeserver   string `validate:"required"`
	Username     string `validate:"required"`
	PasswordFile string `validate:"required"`

	// Chatwoot Authentication
	ChatwootBaseUrl         string `validate:"required"`
	ChatwootFrontBaseUrl    string `validate:"required"`
	ChatwootAccessTokenFile string `validate:"required"`
	ChatwootAccountID       int    `validate:"gt=0"`
	ChatwootInboxID         int    `validate:"gt=0"`

	// Database settings
	DBConnectionString string `validate:"required"`

	// Vitalicia settings
	ChatwootDBConnectionString string `validate:"required"`
	WhatsAppBotUser            string `validate:"required"`
	WhatsAppBridgeUserPrefix   string `validate:"required"`
	MatrixDomain               string `validate:"required"`

	// Bot settings
	AllowMessagesFromUsersOnOtherHomeservers bool
	CanonicalDMPrefix                        string
	BridgeIfMembersLessThan                  int
	RenderMarkdown                           bool

	// Webhook listener settings
	ListenPort int
}

// func (c *Configuration) GetPassword() (string, error) {
// 	log.Debug("Reading password from ", c.PasswordFile)
// 	buf, err := os.ReadFile(c.PasswordFile)
// 	if err != nil {
// 		return "", err
// 	}
// 	return strings.TrimSpace(string(buf)), nil
// }

// func (c *Configuration) GetChatwootAccessToken() (string, error) {
// 	log.Debug("Reading access token from ", c.ChatwootAccessTokenFile)
// 	buf, err := os.ReadFile(c.ChatwootAccessTokenFile)
// 	if err != nil {
// 		return "", err
// 	}
// 	return strings.TrimSpace(string(buf)), nil
// }
