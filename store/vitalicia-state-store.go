package store

import (
	log "github.com/sirupsen/logrus"
)

func (store *StateStore) CreateWelcomeMsgSendState(roomMxid string, chatwootConversationId int) error {
	log.Debugf("Creating welcome message send state for Msxid %s and Chatwoot conversation id %d", roomMxid, chatwootConversationId)
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	insertStatement := `
		INSERT INTO vitalicia_welcome_msg_send_state (matrix_room_id, chatwoot_conversation_id) VALUES ($1, $2)
	`
	if _, err := tx.Exec(insertStatement, roomMxid, chatwootConversationId); err != nil {
		tx.Rollback()
		log.Error(err)
		return err
	}

	return tx.Commit()
}

func (store *StateStore) CountWelcomeMsgSendState(roomMxid string, chatwootConversationId int) (int, error) {
	log.Debugf("Counting welcome message send state for Msxid %s and Chatwoot conversation id %d", roomMxid, chatwootConversationId)

	countStatement := `
		SELECT COUNT(*) FROM vitalicia_welcome_msg_send_state WHERE matrix_room_id=$1 AND chatwoot_conversation_id=$2
	`

	var count int
	var err error

	sqlRow := store.DB.QueryRow(countStatement, roomMxid, chatwootConversationId)
	if err = sqlRow.Scan(&count); err != nil {
		return -1, err
	}
	return count, nil
}

func (store *StateStore) DeleteWelcomeMsgSendState(roomMxid string, chatwootConversationId int) error {
	log.Debugf("Deleting welcome message send state for Mxid %s and Chatwoot conversation id %d", roomMxid, chatwootConversationId)
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	deleteStatement := `
		DELETE FROM vitalicia_welcome_msg_send_state WHERE matrix_room_id=$1 AND chatwoot_conversation_id=$2
	`
	if _, err := tx.Exec(deleteStatement, roomMxid, chatwootConversationId); err != nil {
		tx.Rollback()
		log.Error(err)
		return err
	}

	return tx.Commit()
}
