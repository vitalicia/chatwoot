package store

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
)

type VitaliciaChatwootStateStore struct {
	DB      *sql.DB // Connection to chawoot database
	dialect string
}

func (store *VitaliciaChatwootStateStore) Init(db *sql.DB, dialect string) {
	store.DB = db
	store.dialect = dialect
}

func (store *VitaliciaChatwootStateStore) CreateIdentifier(contactId int, identifier string) error {
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	updateStatement := `UPDATE contacts SET identifier=$1 WHERE id=$2`
	if _, err := tx.Exec(updateStatement, identifier, contactId); err != nil {
		tx.Rollback()
		log.Error(err)
		return err
	}

	return tx.Commit()
}
