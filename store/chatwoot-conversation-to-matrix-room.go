package store

import (
	"database/sql"
	"fmt"

	log "github.com/sirupsen/logrus"
	mid "maunium.net/go/mautrix/id"
)

func (store *StateStore) GetChatwootConversationIDFromMatrixRoom(roomID mid.RoomID) (int, error) {
	// This variable defines the minium interval beetwen messages so that a new conversation will be created, if the interval is less than defined the last conversation closed will be reopen and continued
	const miniumConversationInterval = 12

	row := store.DB.QueryRow(`
		SELECT chatwoot_conversation_id
		  FROM chatwoot_conversation_to_matrix_room
		 WHERE matrix_room_id = $1 
		 AND resolved_at IS NULL`, roomID)
	var chatwootConversationId int
	if err := row.Scan(&chatwootConversationId); err != nil {
		// If no open conversation if found, check if there was a convesation closed in the last x hours
		query := fmt.Sprintf(`
		SELECT chatwoot_conversation_id
		FROM chatwoot_conversation_to_matrix_room
		WHERE matrix_room_id = $1 
		AND resolved_at >= (NOW() - INTERVAL '%d hours' )`, miniumConversationInterval)
		row := store.DB.QueryRow(query, roomID)
		if err := row.Scan(&chatwootConversationId); err != nil {
			return -1, err
		}

		if err := store.reopenConversation(chatwootConversationId); err != nil {
			return -1, err
		}
		
		return chatwootConversationId, nil
	}
	return chatwootConversationId, nil
}

func (store *StateStore) GetMatrixRoomFromChatwootConversation(conversationID int) (mid.RoomID, mid.EventID, error) {
	row := store.DB.QueryRow(`
		SELECT matrix_room_id, most_recent_event_id
		  FROM chatwoot_conversation_to_matrix_room
		 WHERE chatwoot_conversation_id = $1
		 AND resolved_at IS NULL`, conversationID)
	var roomID mid.RoomID
	var mostRecentEventIdStr sql.NullString
	if err := row.Scan(&roomID, &mostRecentEventIdStr); err != nil {
		return "", "", err
	}
	if mostRecentEventIdStr.Valid {
		return roomID, mid.EventID(mostRecentEventIdStr.String), nil
	} else {
		return roomID, mid.EventID(""), nil
	}
}

func (store *StateStore) reopenConversation(chatwootConversationId int) error {
	log.Debugf("Reopening conversation with id: %d", chatwootConversationId)
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	update := `
		UPDATE chatwoot_conversation_to_matrix_room
		SET resolved_at = NULL
		WHERE chatwoot_conversation_id = $1
	`
	if _, err := tx.Exec(update, chatwootConversationId); err != nil {
		tx.Rollback()
		log.Error(err)
		return err
	}

	return tx.Commit()
}

func (store *StateStore) UpdateMostRecentEventIdForRoom(roomID mid.RoomID, mostRecentEventID mid.EventID) error {
	log.Debugf("Setting most recent event ID for %s to %s", roomID, mostRecentEventID)
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	update := `
		UPDATE chatwoot_conversation_to_matrix_room
		SET most_recent_event_id = $2
		WHERE matrix_room_id = $1
	`
	if _, err := tx.Exec(update, roomID, mostRecentEventID); err != nil {
		tx.Rollback()
		log.Error(err)
		return err
	}

	return tx.Commit()
}

func (store *StateStore) UpdateConversationIdForRoom(roomID mid.RoomID, conversationID int) error {
	log.Debug("Upserting row into chatwoot_conversation_to_matrix_room")
	tx, err := store.DB.Begin()
	if err != nil {
		tx.Rollback()
		return err
	}

	upsert := `
		INSERT INTO chatwoot_conversation_to_matrix_room (matrix_room_id, chatwoot_conversation_id)
			VALUES ($1, $2)
		ON CONFLICT (matrix_room_id) DO UPDATE
			SET chatwoot_conversation_id = $2, resolved_at = NULL
	`
	if _, err := tx.Exec(upsert, roomID, conversationID); err != nil {
		tx.Rollback()
		return err
	}

	return tx.Commit()
}
