#!/bin/sh

if [[ -z "$GID" ]]; then
	GID="$UID"
fi

exec su-exec $UID:$GID /usr/bin/chatwoot
