package main

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
	"gitlab.com/beeper/chatwoot/chatwootapi"
	"maunium.net/go/mautrix/id"
	mid "maunium.net/go/mautrix/id"
)

func isWelcomeMsgEnabled(db *sql.DB, inboxId int) (bool, error) {
	var err error
	if db, err = connectToChatwootDB(); err != nil {
		return false, err
	}

	var row *sql.Row

	log.Printf("Check if welcome message is enabled for inboxId %d", inboxId)
	if row = db.QueryRow("SELECT greeting_enabled FROM inboxes WHERE id=$1", inboxId); row.Err() != nil {
		return false, row.Err()
	}
	var welcomeMsgEnabled bool
	row.Scan(&welcomeMsgEnabled)
	return welcomeMsgEnabled, nil
}

func sendWelcomeMessage(messageCreated chatwootapi.MessageCreated, matrixRoomId mid.RoomID) error {

	var err error
	var db *sql.DB
	if db, err = connectToChatwootDB(); err != nil {
		return err
	}
	msgEnabled := false
	if msgEnabled, err = isWelcomeMsgEnabled(db, messageCreated.Conversation.InboxID); err != nil {
		return err
	}
	if !msgEnabled {
		log.Info("Welcome message is not enabled for this account.")
		return nil
	}

	var welcomeMsgSendStateCount int
	var matrixRoomIdStr = matrixRoomId.String()
	log.Info("Checking if the welcome message was not sent yet for Matrix room %s and Chatwoot conversation id %d", matrixRoomIdStr, messageCreated.Conversation.ID)
	if welcomeMsgSendStateCount, err = stateStore.CountWelcomeMsgSendState(matrixRoomIdStr, messageCreated.Conversation.ID); err != nil {
		return err
	}

	if welcomeMsgSendStateCount != 1 {
		return nil
	}

	log.Info("Reading welcome message from Chatwoot database")
	var welcomeMessage string
	if welcomeMessage, err = readWelcomeMessage(messageCreated.Conversation.InboxID); err != nil {
		return err
	}

	if welcomeMessage == "" {
		log.Warn("Welcome message not set in Chatwoot database.")
	} else {
		log.Info("Sending welcome message to Matrix room %s", matrixRoomIdStr)
		if _, err = client.SendText(id.RoomID(matrixRoomIdStr), welcomeMessage); err != nil {
			return err
		}
	}

	log.Info("Deleting welcome message send state for Matrix room %s and Chatwoot conversation id %d", matrixRoomIdStr, messageCreated.Conversation.ID)
	stateStore.DeleteWelcomeMsgSendState(matrixRoomIdStr, messageCreated.Conversation.ID)

	return nil
}

func readWelcomeMessage(inboxId int) (string, error) {
	var db *sql.DB
	var err error
	if db, err = connectToChatwootDB(); err != nil {
		return "", err
	}

	var initStr = ""
	var welcomeMessage *string = &initStr
	sqlRow := db.QueryRow("SELECT greeting_message FROM inboxes WHERE id=$1 AND greeting_message IS NOT NULL", inboxId)
	if err = sqlRow.Scan(welcomeMessage); err != nil {
		if err == sql.ErrNoRows {
			log.Info("No rows in result set")
			return *welcomeMessage, nil
		}
		return "", err
	}

	return *welcomeMessage, nil
}
