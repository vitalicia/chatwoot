How to debug this project on your machine

Note: This does not work on Windows.

1) Go to the Chatwoot super_admin and copy your token
2) Create file ```dev.chatwoot-token``` and add the token from step 1 to it
3) Create the user ```chatwoot-bridge-bot``` in Matrix
4) Create file ```dev.matrix-password``` and add the password from step 3 to it
5) ```git clone https://github.com/go-delve/delve```
6) ```rm -rf delve/.git```
7) Build Docker image by running ```dev.docker-build.sh```
8) Start Docker container by running ```dev.start-container.sh```
9) In the container cd to de directory where the project is. You can find the directory running ```find / -iname dev.debug.sh```
10) Run ```dev.debug.sh```
11) Start debug session in Visual Studio
