#!/bin/ash

go build -o /usr/bin/chatwoot -gcflags="all=-N -l"

if [ $? -ne 0 ]; then
    echo "Build failed."
    exit 1
else
  echo 'Build successful.'
fi

echo 'Starting debug...'
# /go/bin/dlv --listen=:2345 --headless=true --api-version=2 exec /usr/bin/chatwoot
dlv --listen=:2345 --headless=true --api-version=2 exec /usr/bin/chatwoot
