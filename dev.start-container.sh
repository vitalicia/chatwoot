container_name='chatwoot-matrix-bridge'
image_name='vitalicia/chatwoot-matrix-bot:dev'

docker rm -f $container_name

docker run -it --name $container_name -v `pwd`:`pwd` --network chatwoot_default -p 2345:2345 -p 8082:8080 $image_name /bin/ash
