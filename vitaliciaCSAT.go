package main

import (
	"database/sql"

	log "github.com/sirupsen/logrus"
)

type FindCSATUUIDParams struct {
	conversationId int
	inboxId        int
	accountId      int
	contactId      int
	bridgeDbConnection  *sql.DB
}

func getCsatURL(params FindCSATUUIDParams) (string, error) {
	var db *sql.DB
	var err error
	if db, err = connectToChatwootDB(); err != nil {
		return "", err
	}

	csatEnabled := false
	csatEnabled, err = isCsatEnabled(params.inboxId, db)
	if !csatEnabled {
		log.Info("CSAT is not enabled for this account")
		return "", nil
	}

	csatAlreadySent, err := checkCsatAlreadySent(params.conversationId, params.bridgeDbConnection)
	if csatAlreadySent {
		log.Info("CSAT has already been sent for this conversation")
		return "", nil
	}

	var row *sql.Row
	var count int
	if row = db.QueryRow("SELECT COUNT(*) FROM csat_survey_responses WHERE conversation_id=$1 AND account_id=$2 AND contact_id=$3", params.conversationId, params.accountId, params.contactId); row.Err() != nil {
		return "", row.Err()
	}
	if err = row.Scan(&count); err != nil {
		return "", err
	}
	if count > 0 {
		log.Info("CSAT was already answered.")
		return "", nil
	}

	var uuid string
	if uuid, err = readCsatUUID(db, params.conversationId, params.inboxId); err != nil {
		return "", err
	}

	csatUrl := configuration.ChatwootBaseUrl + "/survey/responses/" + uuid
	return csatUrl, nil
}

func readCsatUUID(db *sql.DB, conversationId int, inboxId int) (string, error) {
	var row *sql.Row
	if row = db.QueryRow("SELECT uuid FROM conversations WHERE display_id = $1 AND inbox_id=$2", conversationId, inboxId); row.Err() != nil {
		return "", row.Err()
	}
	var uuid string
	row.Scan(&uuid)
	return uuid, nil
}

func isCsatEnabled(inboxId int, db *sql.DB) (bool, error) {
	var row *sql.Row

	log.Printf("Check if CSAT is enabled for inboxId %d", inboxId)
	if row = db.QueryRow("select csat_survey_enabled from inboxes where id=$1", inboxId); row.Err() != nil {
		return false, row.Err()
	}
	var csatEnabled bool
	row.Scan(&csatEnabled)
	return csatEnabled, nil
}

func markCsatSent(conversationId int, db *sql.DB) (bool, error){
	var row *sql.Row

	query := ` INSERT INTO vitalicia_csat_msg_send_state (chatwoot_conversation_id) VALUES ($1)`
	if row = db.QueryRow(query, conversationId); row.Err() != nil {
		return false, row.Err()
	}

	return true, nil
}

func checkCsatAlreadySent(conversationId int, db *sql.DB) (bool, error) {
	var row *sql.Row
	var id int

	log.Printf("Check if CSAT has already been sent for conversation with id: %d", conversationId)
	query := `
		SELECT chatwoot_conversation_id 
		FROM vitalicia_csat_msg_send_state
		WHERE chatwoot_conversation_id=$1
	`
	if row = db.QueryRow(query, conversationId); row.Err() != nil {
		return false, row.Err()
	}

	err := row.Scan(&id)

	if err == sql.ErrNoRows {
		return false, nil
	}else if err != nil {
		log.Warn("Error checking if CSAT was already sent: %s", err)
		return false, err
	}

	return true, nil
}
