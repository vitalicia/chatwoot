FROM golang:1-alpine3.14 AS builder

RUN apk add --no-cache git ca-certificates build-base su-exec olm-dev

# Clone Delve before building the image -> git clone https://github.com/go-delve/delve

COPY delve /delve
WORKDIR /delve
RUN go install github.com/go-delve/delve/cmd/dlv

WORKDIR /Users/rodrigotenorio/Documents/code/vitalicia/matrix-chatwoot-mirroring-bot/chatwoot

EXPOSE 2345 8080
